#include <SFML/Graphics.hpp>
#include <time.h>
#include "scene.h"

int main(int argc, char *argv[])
{
	srand(time(NULL));
	sf::RenderWindow window(sf::VideoMode(650, 650), "Snake");
	sf::Sprite	sprite_background;
	sf::Texture texture_background;
	sf::Event	event;
	Scene		scene;

	texture_background.loadFromFile("img/background.png");
	sprite_background.setTexture(texture_background);
	window.setFramerateLimit(120);
	while(window.isOpen())
	{
		while(window.pollEvent(event))
		{
			scene.event(event, window);//on envoie les events a la scene
		}
		window.clear(sf::Color(200,150,0));
		window.draw(sprite_background);

		scene.dessine(window);//la scene se dessine
		window.display();
	}
	return 0;
}
