using sf;

enum Velocity {
	UP, DOWN, LEFT, RIGHT
}

public class Game {
	public Game () {
		font = new Font ("./font/Answer.ttf");
		score = new Text () {
			font		= font,
		};
		new_game ();
	}

	void new_game () {
		activity = GAME;
		u_score = 0;
		is_gameover = false;
		// init snake
		snake = {
			new Cube({0,180,0}, {325, 400}),
			new Cube({0,100,0}, {300, 400}),
			new Cube({0,100,0}, {275, 400})
		};
		score.setCharacterSize(32);
		score.position	= {500, 580};
		apple = new Apple();
		velocity = RIGHT;
		score.rotation = 0f;
	}

	void gameover() {
		score.setPosition({120, 315});
		score.setCharacterSize(110);
		score.rotate (-12.0f);
		is_gameover = true;
		activity = OVER;
	}

	public void draw (RenderWindow window) {
		/* loop move (Speed) */
		if (timer.elapsed() > 0.05) {
			move();
			timer.reset ();
		}

		/* Snake drawing */
		foreach (unowned var i in snake) {
			i.draw (window);
		}
		snake[0].draw(window);

		/* Apple drawing */
		apple.draw (window);

		/* Score drawing */
		score.draw (window);
	}
	
	public void event (Event event, RenderWindow window) {
		switch (event.key.code) {
			case KeyCode.Up:
				if (velocity != DOWN)
					velocity = UP;
				break;
			case KeyCode.Down:
				if (velocity != UP)
					velocity = DOWN;
				break;
			case KeyCode.Right:
				if (velocity != LEFT)
					velocity = RIGHT;
				break;
			case KeyCode.Left:
				if (velocity != RIGHT)
					velocity = LEFT;
				break;
			case KeyCode.Enter:
				if (is_gameover)
					new_game ();
				break;
			default:
				break;
		}
	}

	void move () {
		if (is_gameover)
			return ;
		Vector2f tmp;
	
		tmp = snake[0].position;
		switch (velocity) {
			case RIGHT:
				snake[0].move({25, 0});
				break;
			case LEFT:
				snake[0].move({-25, 0});
				break;
			case UP:
				snake[0].move({0, -25});
				break;
			case DOWN:
				snake[0].move({0, 25});
				break;
		}
		for (var i = 1; i < snake.length ; ++i) {
			Vector2f tmp2 = snake[i].position;
			snake[i].position = tmp;
			tmp = tmp2;
		}

		/* Check Collide */
		var snake_bounds = snake[0].getGlobalBounds();
		if (snake_bounds.intersects (apple.getGlobalBounds ())) {
			apple.randomize ();
			snake += new Cube({0, 100, 0}, {-500, -500});
			u_score++;
			score.string = @"Score: $u_score";
		}
		
		for (var i = 1; i < snake.length ; ++i) {
			if (snake_bounds.intersects (snake[i].getGlobalBounds ())) {
				gameover ();
				return ;
			}
		}
		if (snake_bounds.left <= 0 || snake_bounds.left >= 25*25 || snake_bounds.top <= 0 || snake_bounds.top >= 25*25)
		{
			gameover ();
			return ;
		}
		
		if (snake_bounds.intersects (apple.getGlobalBounds ())) {
			apple.randomize ();
			snake += new Cube({0, 100, 0}, {-500, -500});
			++u_score;
		}
	}

	/* Attributs */
	Timer timer = new Timer();
	Velocity velocity = RIGHT;
	uint _uscore;
	uint u_score	{
		get {
			return _uscore;
		}
		set {
			_uscore = value;
			score.string = @"Score: $value";
		}
	}

	bool is_gameover;
	private Apple	apple;
	private Cube	[]snake;
	private Font	font;
	private Text	score;
}
