using sf;

public enum Activity {
	MENU,
	GAME,
	PAUSE,
	OVER
}

public Activity activity;

public class Scene {
	public Scene() {
		menu = new Menu();
		game = new Game();
		activity = MENU;

	}

	public void draw (RenderWindow window) {
		switch (activity) {
			case MENU:
				menu.draw (window);
				break;
			case OVER:
				game.draw (window);
				menu.draw (window);
				break;
			case GAME:
				game.draw (window);
				break;
			case PAUSE:
				break;
		}
	}

	public void event (Event event, RenderWindow window) {
		switch (activity) {
			case OVER:
			case GAME:
				game.event (event, window);
				break;
			case PAUSE:
				break;
			case MENU:
				menu.event (event, window);
				break;
		}
	}

	Menu menu;
	Game game;
}

