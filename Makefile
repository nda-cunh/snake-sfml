SRC= Apple.vala Cube.vala Game.vala main.vala Menu.vala Scene.vala
NAME=snake
all:
	valac ${SRC} -X -w sfml.vapi -X -lcsfml-graphics -X -lcsfml-window -X -lcsfml-system  -o ${NAME}

run:all
	./$(NAME)
